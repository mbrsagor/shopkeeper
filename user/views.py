from django.contrib.auth import logout
from rest_framework import views, status, permissions, generics
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.response import Response

from .models import User, Profile
from .serializers import UserCreateSerializer, UserSerializer, ProfileSerializer, PasswordChangeSerializer, \
    CustomTokenObtainPairSerializer
from utils.responses import prepare_create_success_response, prepare_error_response, prepare_success_response
from validate.auth_validation import password_validation


# User Registration API
class UserCreateAPIView(views.APIView):
    permission_classes = [permissions.AllowAny, ]

    def post(self, request):
        validation_error = password_validation(request.data)
        if validation_error is not None:
            return Response(prepare_error_response(validation_error), status=status.HTTP_400_BAD_REQUEST)
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(prepare_create_success_response(serializer.data), status=status.HTTP_201_CREATED)
        return Response(prepare_error_response(serializer.errors), status=status.HTTP_400_BAD_REQUEST)


# User Login API
class LoginAPIView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


# User Profile API
class ProfileAPIView(views.APIView):
    permission_classes = [permissions.IsAuthenticated, ]

    def get(self, request):
        try:
            queryset = Profile.objects.get(id=self.request.user.id)
            serializer = ProfileSerializer(queryset)
            return Response(prepare_success_response(serializer.data), status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            queryset = User.objects.get(id=self.request.user.id)
            serializer = UserSerializer(queryset)
            return Response(prepare_success_response(serializer.data), status=status.HTTP_200_OK)


# Profile update
class ProfileUpdateView(views.APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, pk):
        try:
            return Profile.objects.get(id=pk)
        except Profile.DoesNotExist:
            return None

    def put(self, request, pk):
        profile = self.get_object(pk)
        if profile is not None:
            serializer = ProfileSerializer(profile, data=request.data)
            if serializer.is_valid():
                serializer.save(user=request.user)
                return Response(prepare_create_success_response(serializer.data), status=status.HTTP_201_CREATED)
            return Response(prepare_error_response(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(prepare_error_response("No user found for this ID"), status=status.HTTP_400_BAD_REQUEST)


# Change Password
class ChangePasswordView(generics.UpdateAPIView):
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = PasswordChangeSerializer
