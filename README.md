# shopkeeper
> ``Shopkeeper`` is a online virtual shop management system portal backend web application

The following steps will walk you thru installation on a Mac. Linux should be similar. It's also possible to develop on a Windows machine, but I have not documented the steps. If you've developed the Django apps on Windows, you should have little problem getting up and running.

### Run the application in your local dev server.

###### Prerequisites
- Python3.8
- psql (PostgreSQL) 13.2
- Al least know Django & Django DRF 

Copy the ``.env-sample`` rename ``.env`` otherwise you can't run the server.

> Open your terminal then, Please following the instructions for running the application in your local server.

```base
git clone https://github.com/mbrsagor/shopkeeper.git
cd shopkeeper
virtualenv venv --python=python3.8
source venv/bin/activate
./manage.py makemigrations auth
./manage.py migrate auth
./manage.py migrate
./manage.py createsuperuser
./manage.py runserver
```

> There are two branches here: example ->
```bash
git branch -a
git branch leadmanager
```
