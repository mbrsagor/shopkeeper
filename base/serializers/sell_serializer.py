from rest_framework import serializers
from base.models.sell import Sell


class SellSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sell
        fields = [
            'id', 'customer', 'items', 'payment', 'due_date',
            'created_at', 'updated_at'
        ]
