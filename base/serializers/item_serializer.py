from rest_framework import serializers
from base.models.item import Category, Item, Brand


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = [
            'id', 'name', 'parent', 'created_at', 'updated_at', 'image'
        ]


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = [
            'id', 'name', 'created_at', 'updated_at', 'image'
        ]


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        read_only_fields = ('user',)
        fields = [
            'id',
            'user',
            'item_name',
            'category_name',
            'brand_name',
            'item_type',
            'price',
            'discount_price',
            'purses_price',
            'status',
            'quantity',
            'serializer_number',
            'is_available',
            'description',
            'total_price',
            'total_purse_price',
            'total_discount_price',
            'image',
            'created_at',
            'updated_at'
        ]
