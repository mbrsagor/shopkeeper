from rest_framework import views, status, permissions
from rest_framework.response import Response

from base.models.sell import Sell
from base.serializers.sell_serializer import SellSerializer


class SellAPIView(views.APIView):
    permission_classes = [permissions.IsAdminUser]

    def get(self, request):
        sell_instance = Sell.objects.all()
        if sell_instance:
            serializer = SellSerializer(sell_instance, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Something went to wrong'}, status=status.HTTP_404_NOT_FOUND)
