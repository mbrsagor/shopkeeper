from django.urls import path
from rest_framework.routers import DefaultRouter
from base.views import item_view
from base.views import sell_view

router = DefaultRouter()

router.register('category', item_view.CategoryViewSet)
router.register('brand', item_view.BrandViewSet)
router.register('item', item_view.ItemViewSet)

urlpatterns = [
    path('sell/', sell_view.SellAPIView.as_view()),
] + router.urls
