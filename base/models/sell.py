import datetime
from django.db import models

from user.models import User
from base.models.base import BaseEntity
from base.models.item import Item
from utils.enum import PAYMENT


class Sell(BaseEntity):
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='customers')
    items = models.ManyToManyField(Item, related_name='sellItem')
    payment = models.IntegerField(choices=PAYMENT.payment_choices(), default=PAYMENT.CASH.value)
    due_date = models.DateField(datetime.datetime.today())

    def __str__(self):
        return self.customer.username
