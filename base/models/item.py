from django.db import models

from user.models import User
from base.models.base import BaseEntity
from utils.enum import TYPES, STATUS


class Category(BaseEntity):
    name = models.CharField(max_length=50, unique=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, related_name='parentCategory', blank=True, null=True)
    image = models.ImageField(upload_to='category', blank=True, null=True)

    def __str__(self):
        return self.name


class Brand(BaseEntity):
    name = models.CharField(max_length=90, unique=True)
    image = models.ImageField(upload_to='brand', blank=True, null=True)

    def __str__(self):
        return self.name


class Item(BaseEntity):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='itemOwner')
    item_name = models.CharField(max_length=120)
    category_name = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='itemCategory')
    brand_name = models.ForeignKey(Brand, on_delete=models.CASCADE, related_name='itemBrand')
    item_type = models.IntegerField(choices=TYPES.select_types(), default=TYPES.PCS.value)
    price = models.DecimalField(default=0.0, max_digits=12, decimal_places=6)
    discount_price = models.DecimalField(default=0.0, max_digits=12, decimal_places=6)
    purses_price = models.DecimalField(default=0.0, max_digits=12, decimal_places=6)
    status = models.IntegerField(choices=STATUS.get_status(), default=STATUS.BUY.value)
    quantity = models.IntegerField(default=0)
    serializer_number = models.CharField(max_length=12, unique=True)
    is_available = models.BooleanField(default=True)
    description = models.TextField(default='')
    image = models.ImageField(upload_to='item', blank=True, null=True)

    def __str__(self):
        return f"{self.item_name[:20]}: Price: {self.price}"

    def total_price(self):
        return int(self.price) * self.quantity

    def total_purse_price(self):
        return int(self.price) * self.purses_price

    def total_discount_price(self):
        return int(self.price) * self.discount_price
