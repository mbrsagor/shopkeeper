from django.contrib import admin
from base.models.item import Category, Brand, Item
from base.models.sell import Sell

admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Item)
admin.site.register(Sell)
